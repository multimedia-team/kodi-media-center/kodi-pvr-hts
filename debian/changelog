kodi-pvr-hts (21.2.6+ds-1) unstable; urgency=high

  * New upstream version 21.2.6+ds
  * d/copyright: Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 27 Jan 2025 08:45:52 +0000

kodi-pvr-hts (21.2.5+ds-2) unstable; urgency=critical

  * Mass fix: d/watch: Allow multiple addon suites to be checked

 -- Vasyl Gello <vasek.gello@gmail.com>  Tue, 20 Aug 2024 12:13:38 +0000

kodi-pvr-hts (21.2.5+ds-1) unstable; urgency=medium

  * New upstream version 21.2.5+ds

 -- Vasyl Gello <vasek.gello@gmail.com>  Fri, 09 Aug 2024 07:57:11 +0000

kodi-pvr-hts (21.2.1+ds-1) experimental; urgency=medium

  * New upstream version 21.2.1+ds
  * Branch out experimental
  * Modernize package
  * New release 20.3.0+ds1-1
  * Finalize changelog
  * d/gbp.conf: Fix debian and upstream branches
  * Import Debian packaging from debian/sid
  * Prepare for v21 "Omega"
  * Bump copyright years
  * d/control: Ensure Vcs-Git points to correct branch

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 09 Mar 2024 15:10:00 +0000

kodi-pvr-hts (20.7.2+ds-1) unstable; urgency=medium

  * New upstream version 20.7.2+ds

 -- Vasyl Gello <vasek.gello@gmail.com>  Tue, 05 Mar 2024 17:01:19 +0000

kodi-pvr-hts (20.7.0+ds-1) unstable; urgency=medium

  * New upstream version 20.7.0+ds
  * Bmp copyright years
  * d/watch: Fix repack suffix
  * d/gbp.conf: Fix debian and upstream branches

 -- Vasyl Gello <vasek.gello@gmail.com>  Sun, 04 Feb 2024 08:40:32 +0000

kodi-pvr-hts (20.6.0+ds1-1) unstable; urgency=medium

  * New upstream version 20.6.0+ds1
  * Fix lintian warnings
  * d/watch: switch to git tags

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 15 Oct 2022 16:54:36 +0000

kodi-pvr-hts (20.3.0+ds1-1) unstable; urgency=medium

  * New upstream version 20.3.0+ds1
  * Prepare for v20 in unstable

 -- Vasyl Gello <vasek.gello@gmail.com>  Thu, 04 Aug 2022 09:54:42 +0000

kodi-pvr-hts (19.0.6+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.6+ds1
  * Modernize package

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 21 Mar 2022 17:48:54 +0000

kodi-pvr-hts (19.0.2+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.2+ds1

 -- Vasyl Gello <vasek.gello@gmail.com>  Thu, 28 Oct 2021 11:25:24 +0000

kodi-pvr-hts (19.0.0+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.0+ds1
  * Disable LTO to make build reproducible

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 13 Oct 2021 22:13:47 +0000

kodi-pvr-hts (8.4.0+ds1-1) unstable; urgency=medium

  * New upstream version 8.4.0+ds1
  * Bump standard version (no changes required)

 -- Vasyl Gello <vasek.gello@gmail.com>  Thu, 26 Aug 2021 14:53:47 +0000

kodi-pvr-hts (8.3.4+ds1-1) unstable; urgency=medium

  * New upstream version 8.3.4+ds1
  * Restrict watchfile to current stable Kodi codename

 -- Vasyl Gello <vasek.gello@gmail.com>  Sun, 15 Aug 2021 21:31:50 +0000

kodi-pvr-hts (8.3.0+ds1-1) unstable; urgency=medium

  * New upstream version 8.3.0+ds1
  * Fix github ref

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 08 May 2021 22:06:28 +0000

kodi-pvr-hts (8.2.2+ds1-1) unstable; urgency=medium

  * New upstream version 8.2.2+ds1

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 20 Jan 2021 14:46:54 +0000

kodi-pvr-hts (8.2.1+ds1-1) unstable; urgency=medium

  * New upstream version 8.2.1+ds1
  * Force override libdir paths
  * Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 20 Jan 2021 06:23:56 +0000

kodi-pvr-hts (8.1.2+ds1-1) unstable; urgency=medium

  * New upstream version 8.1.2+ds1 (Closes: #978076)
    + Repack out vendored dependencies
  * d/rules:
    + Use architecture.mk for DEB_HOST_MULTIARCH
    + Enable hardening flags
  * Update d/watch
  * d/control:
    + Remove Balint Reczey from Uploaders per his wish, and add myself
    + Use dh-sequence-kodiaddon
    + Drop kodiplatform build-dependency
    + Update Vcs-* for the move to 'kodi-media-center' subgroup
    + Bump Standards-Version to 4.5.1, no changes needed
    + Bump debhelper compat level to 13, use debhelper-compat and drop d/compat
  * Update d/copyright
  * Add new d/upstream/metadata

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 21 Dec 2020 15:52:15 +0000

kodi-pvr-hts (4.4.20-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Balint Reczey ]
  * Add debian/watch file
  * Add basic Salsa CI configuration
  * Bump build dependency version of kodi-addons-dev and libkodiplatform-dev
  * New upstream version 4.4.20

 -- Balint Reczey <rbalint@ubuntu.com>  Sat, 21 Mar 2020 16:38:25 +0100

kodi-pvr-hts (3.4.13-1) unstable; urgency=medium

  * New upstream version 3.4.13
  * Depend on PVR API version of Kodi we built the addon with

 -- Tobias Grimm <etobi@debian.org>  Wed, 28 Dec 2016 10:55:43 +0100

kodi-pvr-hts (3.4.7+git20161104-1) unstable; urgency=medium

  * New upstream version
  * Build-depend on libkodiplatform-dev (>= 17.1.0) and
    kodi-addons-dev (>= 17.0~beta3+dfsg1-2)
  * Dropped obsolete libcec-platform.patch
  * Standards version 3.9.8
  * Updated kodi dependency

 -- Tobias Grimm <etobi@debian.org>  Fri, 04 Nov 2016 20:28:36 +0100

kodi-pvr-hts (2.2.14-1) unstable; urgency=medium

  * New upstream release
  * Now using libkodiplatform > 17.0.0 and libp8-platform (instead of
    libcec-platform)

 -- Tobias Grimm <etobi@debian.org>  Fri, 04 Mar 2016 12:22:33 +0100

kodi-pvr-hts (2.1.18-1) unstable; urgency=medium

  * Initial release (replaces xbmc-pvr-addons / xbmc-pvr-tvheadend-hts)

 -- Tobias Grimm <etobi@debian.org>  Mon, 23 Nov 2015 21:28:14 +0100
